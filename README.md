# Vehicle Actuation - Autoware Interface

Vehicle Command - publishes ROS message to network interface using mqtt

Vehicle Status - publishes messages from network interface via mqtt as a ROS  message


## Running the  vehicle command sequence with test client
```
rosrun actuation-autoware-bridge vehicle_cmd_node
rosrun actuation-autoware-bridge actuation_vehicle_cmd_node_test
rosrun actuation-autoware-bridge pc_vehicle_cmd_node_test
```
## Running the vehicle status sequence with test client
```
rosrun actuation-autoware-bridge  vehicle_status_node
rosrun actuation-autoware-bridge  pc_vehicle_status_node_test
rosrun actuation-autoware-bridge actuation_vehicle_status_node_test
```