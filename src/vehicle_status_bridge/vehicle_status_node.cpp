#include <mqtt_ros_cpp/subscriber.hpp>

#include "ros/ros.h"
#include "std_msgs/String.h"

#include "autoware_msgs/VehicleCmd.h"
#include <autoware_msgs/VehicleStatus.h>
#include "time.h"

#include <sstream>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "ros_mqtt_test");

  ros::NodeHandle n;

  mqttSubscriber mySubscriber("mqtt_subscriber", "vehicle_status",  "localhost", 1883, 50, n);

  ros::Rate loop_rate(20);

  int count = 0;
//   ros::Time current_time = ros::Time::now();

  while (ros::ok())
  {
    // ROS_INFO("VehicleCmd [ROS] published");

    ros::spinOnce();

    loop_rate.sleep();
    ++count;
  }

  return 0;
}