#include <mqtt_ros_cpp/publisher.hpp>
#include <ros/serialization.h>
#include "nlohmann/json.hpp"
#include "autoware_msgs/VehicleCmd.h"
#include "time.h"

using json = nlohmann::json;

mqttPublisher vehicleStatusPublisher("mqtt_publisher", "vehicle_status",  "localhost", 1883);


int main(int argc, char *argv[])
{
  ros::init(argc, argv, "actuation_vehicle_status_node_test");
  ros::NodeHandle nh;
  ros::Rate loop_rate(10);
  int count = 0;
  json j;
  j["emergency"] = false;
  j["pi"] = 3.141;
  j["name"] = "TridentOne";
  j["accel"] = 6;
  
  // ROS_INFO_STREAM("mqttPublisher: Json j = " << j << "\n published");
  while (ros::ok())
  {
    vehicleStatusPublisher.send_message(j.dump().c_str());

    ros::spinOnce();
    loop_rate.sleep();
    count++;
  }
  ROS_INFO("mqttPublisher: Exiting");
  return 0;
}
