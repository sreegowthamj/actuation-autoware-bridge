#include <mqtt_ros_cpp/publisher.hpp>
#include <ros/serialization.h>
#include "nlohmann/json.hpp"
#include "autoware_msgs/VehicleStatus.h"
#include "time.h"

using json = nlohmann::json;

void vehicleStatusCallback(const autoware_msgs::VehicleStatus& msg)
{
  ROS_INFO("[vehicleStatusCallback] heard: time = %d, msg.drivemode = %d", msg.header.stamp.sec, msg.drivemode );
  // Do the write from a seperate thread
}

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "vehicle_cmd_node");
  ros::NodeHandle nh;
  ros::Subscriber sub = nh.subscribe("/vehicle_status", 1000, vehicleStatusCallback);
  ros::Rate loop_rate(100);
  int count = 0;
  
  // ROS_INFO_STREAM("mqttPublisher: Json j = " << j << "\n published");
  while (ros::ok())
  {
    // myPublisher.send_message(std::to_string(count).c_str());
    // myPublisher.send_message(j.dump().c_str());
    ros::spinOnce();
    loop_rate.sleep();
    count++;
  }
  ROS_INFO("mqttPublisher: Exiting");
  return 0;
}
