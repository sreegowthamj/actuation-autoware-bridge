#include <mqtt_ros_cpp/subscriber.hpp>
#include "nlohmann/json.hpp"
#include "ros/ros.h"
#include "std_msgs/String.h"

#include "autoware_msgs/VehicleCmd.h"
#include <autoware_msgs/VehicleStatus.h>
#include "time.h"

using json = nlohmann::json;

mqttSubscriber::mqttSubscriber(const char *id, const char * topic, const char *host, int port, int max_payload):
  mqttManager(id, topic, host, port),
  max_payload_(max_payload)
{
}

mqttSubscriber::mqttSubscriber(const char *id, const char * topic, const char *host, int port, int max_payload, ros::NodeHandle& n):
  mqttManager(id, topic, host, port),
  max_payload_(max_payload)
{
  veh_status_pub = n.advertise<autoware_msgs::VehicleStatus>("/vehicle_status", 1000);

}

void mqttSubscriber::startSubscribe()
{
  this->subscribe(NULL, topic_);
}

void mqttSubscriber::on_subscribe(int mid, int qos_count, const int *granted_qos)
{
  ROS_INFO_STREAM_ONCE("mqttSubscribe - Message (" << mid << ") succeed to subscribe");
}

void mqttSubscriber::on_message(const struct mosquitto_message *message)
{
  int payload_size = max_payload_ + 1;
  char buf[payload_size];
  ROS_INFO_STREAM("mqttSubscriber: a message is received");
    autoware_msgs::VehicleStatus msg;
    msg.drivemode = 3;
    msg.header.stamp = ros::Time::now();

    veh_status_pub.publish(msg);

  if(!strcmp(message->topic, topic_))
  {
    memset(buf, 0, payload_size * sizeof(char));

    /* Copy N-1 bytes to ensure always 0 terminated. */
    memcpy(buf, message->payload, max_payload_ * sizeof(char));
    ROS_INFO_STREAM("mqttSubscriber: message received: "<< buf);
  }
}

void mqttSubscriber::on_connect(int rc)
{
  if ( rc == 0 ) {
    ROS_INFO_ONCE("mqttSubscriber: myMqtt - connected with server");
    this->startSubscribe();
  }else{
    ROS_WARN_STREAM_ONCE("mqttSubscriber: myMqtt - Impossible to connect with server(" << rc << ")");
  }
}
