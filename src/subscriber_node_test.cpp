#include <mqtt_ros_cpp/subscriber.hpp>

#include "ros/ros.h"
#include "std_msgs/String.h"

#include <autoware_msgs/VehicleCmd.h>
#include <autoware_msgs/VehicleStatus.h>
#include "time.h"

#include <sstream>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "ros_mqtt_test");

  ros::NodeHandle n;

  ros::Publisher veh_cmd_pub = n.advertise<autoware_msgs::VehicleCmd>("/vehicle_cmd", 1000);

  ros::Rate loop_rate(20);

  int count = 0;
  while (ros::ok())
  {
    autoware_msgs::VehicleCmd msg;

    msg.accel_cmd.accel = 5;
    msg.brake_cmd.brake = 

    ROS_INFO("%s", msg.data.c_str());

    veh_cmd_pub.publish(msg);

    ros::spinOnce();

    loop_rate.sleep();
    ++count;
  }

  return 0;
}