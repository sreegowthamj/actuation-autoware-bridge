#include <mqtt_ros_cpp/publisher.hpp>
#include <ros/serialization.h>
#include "nlohmann/json.hpp"
#include "autoware_msgs/VehicleCmd.h"
#include "time.h"

using json = nlohmann::json;

mqttPublisher myPublisher("mqtt_publisher", "vehicle_cmd",  "localhost", 1883);

void vehicleCmdCallback(const autoware_msgs::VehicleCmd& msg)
{
  ROS_INFO("[MQTT_PUB] heard: time = %d, acc_data = %d", msg.header.stamp.sec, msg.accel_cmd.accel  );
  // Do the write from a seperate thread
  json j;
  j["emergency"] = false;
  j["pi"] = 3.141;
  j["name"] = "TridentOne";
  j["accel"] = msg.accel_cmd.accel;
  myPublisher.send_message(j.dump().c_str());

}

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "vehicle_cmd_node");
  ros::NodeHandle nh;
  ros::Subscriber sub = nh.subscribe("/vehicle_cmd", 1000, vehicleCmdCallback);
  ros::Rate loop_rate(10);
  int count = 0;
  
  // ROS_INFO_STREAM("mqttPublisher: Json j = " << j << "\n published");
  while (ros::ok())
  {
    // myPublisher.send_message(std::to_string(count).c_str());
    // myPublisher.send_message(j.dump().c_str());
    ros::spinOnce();
    loop_rate.sleep();
    count++;
  }
  ROS_INFO("mqttPublisher: Exiting");
  return 0;
}
