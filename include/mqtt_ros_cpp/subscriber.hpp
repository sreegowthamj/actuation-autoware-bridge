#ifndef SUBSCRIBER_HPP
#define SUBSCRIBER_HPP

#include <mqtt_ros_cpp/mqttManager.hpp>
#include "ros/ros.h"

class mqttSubscriber : public mqttManager
{
public:
  mqttSubscriber(const char *id, const char * topic, const char *host, int port, int max_payload);
  mqttSubscriber(const char *id, const char * topic, const char *host, 
  int port, int max_payload, ros::NodeHandle &n);

  void startSubscribe();

  void on_connect(int rc);
  void on_message(const struct mosquitto_message *message);
  void on_subscribe(int mid, int qos_count, const int *granted_qos);

private:

  int max_payload_;
  ros::Publisher veh_status_pub;
};

#endif //SUBSCRIBER_HPP
